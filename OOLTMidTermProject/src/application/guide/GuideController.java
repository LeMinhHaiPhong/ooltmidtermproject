package application.guide;
import java.io.IOException;
import application.Main;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class GuideController {

	  @FXML
	    public void showVertexGuide() throws IOException {
			FXMLLoader loader = new FXMLLoader();
		    loader.setLocation(Main.class.getResource("guide/VertexGuide.fxml"));
		    AnchorPane layout=loader.load();
		    Stage stage = new Stage();
		    stage.setTitle("VertexGuide");
		    stage.setScene(new Scene(layout));
		    stage.initModality(Modality.APPLICATION_MODAL);
		    stage.showAndWait();
	    }

	    @FXML
	    public void showEdgeGuide() throws IOException {
		    FXMLLoader loader = new FXMLLoader();
		    loader.setLocation(Main.class.getResource("guide/EdgeGuide.fxml"));
	        BorderPane layout=loader.load();
		    Stage stage = new Stage();
		    stage.setTitle("EdgeGuide");
		    stage.setScene(new Scene(layout));
		    stage.initModality(Modality.APPLICATION_MODAL);
	   	    stage.showAndWait();
	    }

	    @FXML
	    public void showBFSGuide() throws IOException {
			FXMLLoader loader = new FXMLLoader();
	   	    loader.setLocation(Main.class.getResource("guide/BFSGuide.fxml"));
	        ScrollPane layout=loader.load();
	        Stage stage = new Stage();
		    stage.setTitle("BFSGuide");
		    stage.setScene(new Scene(layout));
		    stage.initModality(Modality.APPLICATION_MODAL);
		    stage.showAndWait();
	    }

	    @FXML
	    public void showDFSGuide() throws IOException {
	    	FXMLLoader loader = new FXMLLoader();
		    loader.setLocation(Main.class.getResource("guide/DFSGuide.fxml"));
		    ScrollPane layout=loader.load();
		    Stage stage = new Stage();
		    stage.setTitle("DFSGuide");
		    stage.setScene(new Scene(layout));
		    stage.initModality(Modality.APPLICATION_MODAL);
		    stage.showAndWait();
	    }

	    @FXML
	    public void showDijkstraGuide() throws IOException {
	    	FXMLLoader loader = new FXMLLoader();
		    loader.setLocation(Main.class.getResource("guide/DijkstraGuide.fxml"));
		    ScrollPane layout=loader.load();
		    Stage stage = new Stage();
			stage.setTitle("DijkstraGuide");
			stage.setScene(new Scene(layout));
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.showAndWait();
	    }
}
