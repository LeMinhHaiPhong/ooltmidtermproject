package application;
	
import java.io.IOException;
import java.util.Optional;

import application.algorithm.bfs.BfsController;
import application.algorithm.dfs.DfsController;
import application.algorithm.dijkstra.DijkstraController;
import application.graph.Graph;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class Main extends Application {
	
	public static Stage primaryStage;
	public static Stage AddEdgeStage;
	public static Stage EraseEdgeStage;
	public static Stage BfsStage;
	public static Stage DfsStage;
	public static Stage DijkstraStage;
	public static Stage GuideStage;
	public static BorderPane homeLayout;
	private static Graph canvas;
	public static TextField startVertex;

	@Override
	public void start(Stage primaryStage) {
		Main.primaryStage = primaryStage;
		try {
			homeLayout = (BorderPane) FXMLLoader.load(getClass().getResource("Main.fxml"));
			canvas = new Graph();
			canvas.setStyle("-fx-background-color:white");
			homeLayout.setCenter(canvas);
			
			primaryStage.setScene(new Scene(homeLayout));
			primaryStage.setTitle("Graph Algorithm Demonstrator");
			primaryStage.show();
			startVertex = (TextField) ((HBox) ((VBox) ((BorderPane) homeLayout.getRight()).getCenter()).getChildren().get(0)).getChildren().get(1);
		} catch (IOException e) {
			e.printStackTrace();
		}				
	}
	
	public static Graph getCanvas() {
		return Main.canvas;
	}
	
	public static void main(String[] args) {
		launch(args);
	}
	
	public void showBfsView() throws IOException {
		BorderPane _Layout = (BorderPane) FXMLLoader.load(getClass().getResource("algorithm/bfs/BfsView.fxml"));
		BfsStage = new Stage();
		BfsStage.setTitle("BFS");
		BfsStage.setScene(new Scene(_Layout));
		BfsStage.initModality(Modality.APPLICATION_MODAL);
		
		(new BfsController(BfsStage)).runBFS();
		BfsStage.showAndWait();
	}
	
	public void showDfsView() throws IOException {
		BorderPane _Layout = (BorderPane) FXMLLoader.load(getClass().getResource("algorithm/dfs/DfsView.fxml"));
		DfsStage = new Stage();
		DfsStage.setTitle("DFS");
		DfsStage.setScene(new Scene(_Layout));
		DfsStage.initModality(Modality.APPLICATION_MODAL);
		(new DfsController(DfsStage)).runDFS();
		DfsStage.showAndWait();
	}
	
	public void showDijkstraView() throws IOException {
		BorderPane _Layout = (BorderPane) FXMLLoader.load(getClass().getResource("algorithm/dijkstra/DijkstraView.fxml"));
		DijkstraStage = new Stage();
		DijkstraStage.setTitle("Dijkstra");
		DijkstraStage.setScene(new Scene(_Layout));
		DijkstraStage.initModality(Modality.APPLICATION_MODAL);
		(new DijkstraController(DijkstraStage)).runDijkstra();
		DijkstraStage.showAndWait();
	}
	
	public void showAddEdgeView() throws IOException {
		BorderPane _Layout = (BorderPane) FXMLLoader.load(getClass().getResource("graph/edgeModification/AddEdge.fxml"));
		AddEdgeStage = new Stage();
		AddEdgeStage.setTitle("Add/Modify Edge");
		AddEdgeStage.setScene(new Scene(_Layout));
		AddEdgeStage.initModality(Modality.APPLICATION_MODAL);
		AddEdgeStage.showAndWait();
	}
	
	public void showEraseEdgeView() throws IOException {
		BorderPane _Layout = (BorderPane) FXMLLoader.load(getClass().getResource("graph/edgeModification/EraseEdge.fxml"));
		EraseEdgeStage = new Stage();
		EraseEdgeStage.setTitle("Delete Edge");
		EraseEdgeStage.setScene(new Scene(_Layout));
		EraseEdgeStage.initModality(Modality.APPLICATION_MODAL);
		EraseEdgeStage.showAndWait();
	}
	
	public void showGuideView() throws IOException {
		BorderPane layout = (BorderPane) FXMLLoader.load(getClass().getResource("guide/Guide.fxml"));
		GuideStage = new Stage();
		GuideStage.setTitle("Guide");
		GuideStage.setScene(new Scene(layout));
		GuideStage.initModality(Modality.APPLICATION_MODAL);
		GuideStage.showAndWait();
    }
	
	public void showAboutView() throws IOException {
		VBox layout = (VBox) FXMLLoader.load(getClass().getResource("About.fxml"));
		Stage stage = new Stage();
		stage.setTitle("About");
		stage.setScene(new Scene(layout));
		stage.initModality(Modality.APPLICATION_MODAL);
		stage.showAndWait();
    }
	
	public Optional<ButtonType> showError(String message) {
		Alert alert = new Alert(AlertType.ERROR);
		alert.setTitle("Error !");
		alert.setHeaderText(null);
		alert.setContentText(message);
		alert.initModality(Modality.APPLICATION_MODAL);
		return alert.showAndWait();
	}
	
	public Optional<ButtonType> showConfirmation(String message) {
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("Warning !");
		alert.setHeaderText(null);
		alert.setContentText(message);
		alert.initModality(Modality.APPLICATION_MODAL);
		return alert.showAndWait();
	}

}
