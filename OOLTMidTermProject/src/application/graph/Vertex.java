package application.graph;

import java.util.ArrayList;
import java.util.List;

import javafx.scene.Group;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;

public class Vertex extends Group {
		
	private int ID;
	private double x;
	private double y;
	public List<Edge> edges;
	
	public Vertex() {
		super();
		edges = new ArrayList<Edge>();
	}

	public Vertex(double x, double y, int id) {
		this();
		this.x = x;
		this.y = y;
		Circle circle = new Circle(x, y, 10);
		circle.setFill(Color.WHITE);
		circle.setStroke(Color.BLACK);
		this.ID = id;
		Text text = new Text(x - 5, y + 5, "" + id);
		this.getChildren().addAll(circle, text);
	}
	
	public double getx() {
		return this.x;
	}
	
	public double gety() {
		return this.y;
	}
	
	public int getVertexId() {
		return this.ID; 
	}
	
	public void setVertexId(int id) {
		for (Edge e : this.edges) {
			if (e.getU() == this.ID) e.setU(id);
			if (e.getV() == this.ID) e.setV(id);
		}
		this.ID = id;
		((Text) this.getChildren().get(1)).setText("" + id);
	}
	
	public void addEdge(Edge e) {
		this.edges.add(e);
	}
	
	public Vertex clone() {
		Vertex ver = new Vertex(this.x, this.y, this.ID);
		for (Edge e : this.edges) {
			ver.addEdge(e.clone());
		}
		return ver;
	}
}
