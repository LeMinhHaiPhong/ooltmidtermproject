package application.graph;

import java.util.ArrayList;
import java.util.List;

import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Line;
import javafx.scene.text.Text;

public class Graph extends AnchorPane {
	public static final int MAX_NUMBER_OF_VERTICES = 50;
	public static final int MAX_NUMBER_OF_EDGES = 500;
	public int vertexNumber = 0;
	public Vertex[] vertices;
	public List<Edge> edges;
	public boolean isFixed = false;
	private static Vertex _start;
	private static Vertex _end;
	private static Edge _edge;
	
	public Graph() {
		// TODO Auto-generated constructor stub
		super();
		vertices = new Vertex[MAX_NUMBER_OF_VERTICES];
		this.edges = new ArrayList<Edge>();
		
		this.setOnMousePressed(new EventHandler<MouseEvent>() { 	
	        @Override 
	        public void handle(MouseEvent e) {
	        	_start = getVertex(e.getX(), e.getY());
	        	if (_start == null && canAddVertex(e.getX(), e.getY())) {
	        		try {
			        	vertices[vertexNumber] = new Vertex(e.getX(), e.getY(), vertexNumber);
			        	getChildren().add(vertices[vertexNumber]);
			        	vertexNumber++;
		        	}
		        	catch (Exception exept) {
		        		System.err.println("Too many vertices.");
		        	}
	        	}
	        	
	        	if (_start != null) {
	        		_edge = new Edge(e.getX(), e.getY(), e.getX(), e.getY(), 1);
	        		getChildren().add(0, _edge);
	        	}
	        	e.consume();
	        }
	     });
		
		this.setOnMouseReleased(new EventHandler<MouseEvent>() { 	
	        @Override 
	        public void handle(MouseEvent e) {
	        	_end = getVertex(e.getX(), e.getY());
	        	if (_start != null) {
	        		getChildren().remove(_edge);
	        		if (_end != null) {
	        			if (_start.getVertexId() == _end.getVertexId()) eraseVertex(_start.getVertexId()); else
	        			addEdge(_start.getVertexId(), _end.getVertexId(), 1);
	        		}
	        	}
	        }
	     });
		
		this.setOnMouseDragged(new EventHandler<MouseEvent>() { 	
	        @Override 
	        public void handle(MouseEvent e) {
	        	if (_start != null) {
	        		Line _line = (Line) _edge.getChildren().get(0);
	        		_line.setEndX(e.getX());
	        		_line.setEndY(e.getY());
	        		Text _weight = (Text) _edge.getChildren().get(1);
	        		_weight.setX((_line.getStartX() + _line.getEndX())/2);
	        		_weight.setY((_line.getStartY() + _line.getEndY())/2);
	        	}
	        }
	     });
	}
	
	private Vertex getVertex(double d, double e) {
		for (int i = 0; i < vertexNumber; i++) {
    		double dist = (d - vertices[i].getx()) * (d - vertices[i].getx()) + (e - vertices[i].gety()) * (e - vertices[i].gety());
    		if (dist < 100) {
    			return vertices[i];
    		}
    	}
		return null;
	}
	
	private boolean canAddVertex(double d, double e) {
		for (int i = 0; i < vertexNumber; i++) {
    		double dist = (d - vertices[i].getx()) * (d - vertices[i].getx()) + (e - vertices[i].gety()) * (e - vertices[i].gety());
    		if (dist < 400) {
    			return false;
    		}
    	}
		return true;
	}
	
	public int getVertexNumber() {
		return vertexNumber;
	}

	public Vertex getVertex(int id) {
		return vertices[id].clone();
	}

	public void clearGraph() {
		this.getChildren().clear();
		vertexNumber = 0;
		this.edges.clear();
		//this.edges = new ArrayList<Edge>();
	}
	
	public void eraseVertex(int id) {
		this.getChildren().remove(vertices[id]);
		for (int i = 0; i < vertexNumber; i++) {
			eraseEdge(id, i, -1);
		}
		
		for (int i = id + 1; i < vertexNumber; i++) {
			vertices[i - 1] = vertices[i];
			vertices[i - 1].setVertexId(i - 1);
		}
		vertexNumber--;
	}
	
	public void addEdge(int u, int v, int w) {
		Edge e = new Edge(vertices[u].getx(), vertices[u].gety(), vertices[v].getx(), vertices[v].gety(), w);
		e.setU(u);
		e.setV(v);
		vertices[u].addEdge(e);
		vertices[v].addEdge(e);
		this.edges.add(e);
		this.getChildren().add(0, e);
	}
	
	public void eraseEdge(int u, int v, int w) {
		Edge e = new Edge(u, v, w);
		this.edges.remove(e);
		this.vertices[u].edges.remove(e);
		this.vertices[v].edges.remove(e);
		this.getChildren().remove(e);
	}

}
