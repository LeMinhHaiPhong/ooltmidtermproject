package application.graph;

import application.Main;
import javafx.scene.Group;
import javafx.scene.shape.Line;
import javafx.scene.text.Text;

public class Edge extends Group {
	
	private Line line;
	private int weight = 0;
	public Text weightText;
	private int u;
	private int v;

	public Edge() {
		super();
	}
	
	public Edge(double x1,  double y1, double x2, double y2, int weight) {
		this();
		this.line = new Line(x1, y1, x2, y2);
		this.weightText = new Text();
		this.setWeight(weight);
		this.weightText.setX((x1 + x2)/2);
		this.weightText.setY((y1 + y2)/2);
		this.getChildren().addAll(this.line, this.weightText);
	}

	public Edge(int u2, int v2, int w) {
		this(Main.getCanvas().vertices[u2].getx(), Main.getCanvas().vertices[u2].gety(), Main.getCanvas().vertices[v2].getx(), Main.getCanvas().vertices[v2].gety(), w);
		this.setU(u2);
		this.setV(v2);
	}

	public int getWeight() {
		return this.weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
		this.weightText.setText("" + weight);
	}
	
	public int getU() {
		return u;
	}

	public void setU(int u) {
		this.u = u;
	}

	public int getV() {
		return v;
	}

	public void setV(int v) {
		this.v = v;
	}
	
	@Override
	public boolean equals(Object o) {
		try {
			return (this.getU() == ((Edge) o).getU() && this.getV() == ((Edge) o).getV()) || (this.getU() == ((Edge) o).getV() && this.getV() == ((Edge) o).getU());
		}
		catch (ClassCastException e) {
			return false;
		}
	}

	public Edge clone() {
		Edge e = new Edge(line.getStartX(), line.getStartY(), line.getEndX(), line.getEndY(), this.getWeight());
		return e;
	}
}
