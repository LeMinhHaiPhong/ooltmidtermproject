package application.graph.edgeModification;

import java.util.Optional;

import application.Main;
import application.graph.Edge;
import javafx.fxml.FXML;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;

public class AddEdgeController {
	Main main = new Main();
	
	@FXML
	public void buttonOK() {
		GridPane grid = (GridPane) ((BorderPane) Main.AddEdgeStage.getScene().getRoot()).getCenter();
		int u = -1, v = -1, w = -1;
		try {
			u = Integer.valueOf(((TextField) grid.getChildren().get(3)).getCharacters().toString());
			v = Integer.valueOf(((TextField) grid.getChildren().get(4)).getCharacters().toString());
			w = Integer.valueOf(((TextField) grid.getChildren().get(5)).getCharacters().toString());
			if (u < 0 || u >= Main.getCanvas().vertexNumber) 
				main.showError("Invalid start vertex !\nVertex must be an integer between 0 and " + (Main.getCanvas().vertexNumber - 1)); 
			else
			if (v < 0 || v >= Main.getCanvas().vertexNumber) 
				main.showError("Invalid end vertex !\nVertex must be an integer between 0 and " + (Main.getCanvas().vertexNumber - 1)); 
			else
			if (u == v) 
				main.showError("Start vertex and End vertex must be different."); 
			else
			if (w < 0 || w >= 100) main.showError("Invalid edge weight !\nEdge weight must be an integer between 0 and " + 99); 
			else
			if (Main.getCanvas().edges.contains(new Edge(u, v, w))) {
				Optional<ButtonType> result = main.showConfirmation("This edge is already added.\nThis action might change the edge weight.\nAre you sure?");
				if (result.get() == ButtonType.OK) {
					Main.getCanvas().eraseEdge(u, v, w);
					Main.getCanvas().addEdge(u, v, w);
					Main.AddEdgeStage.close();
				}
			} 
			else {
				Main.getCanvas().addEdge(u, v, w);
				Main.AddEdgeStage.close();
			}
		}
		catch (Exception e) {
			if (u < 0 || u >= Main.getCanvas().vertexNumber) 
				main.showError("Invalid start vertex !\nVertex must be an integer between 0 and " + (Main.getCanvas().vertexNumber - 1)); 
			else
			if (v < 0 || v >= Main.getCanvas().vertexNumber) 
				main.showError("Invalid end vertex !\nVertex must be an integer between 0 and " + (Main.getCanvas().vertexNumber - 1)); 
			else
			if (w < 0 || w >= 100) main.showError("Invalid edge weight !\nEdge weight must be an integer between 0 and " + 99); 
			else {
				main.showError("Add edge error.");
			}
		}
	}
	
	@FXML
	public void buttonCancel() {
		Main.AddEdgeStage.close();
	}
	
	@FXML
	public void keyPressed(KeyEvent e) {
		if (e.getCode() == KeyCode.ENTER) {
			buttonOK();
		}
	}
	
}
