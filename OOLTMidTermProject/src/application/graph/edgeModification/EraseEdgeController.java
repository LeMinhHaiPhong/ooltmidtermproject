package application.graph.edgeModification;

import java.util.Optional;

import application.Main;
import application.graph.Edge;
import javafx.fxml.FXML;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;

public class EraseEdgeController {
	Main main = new Main();
	
	@FXML
	public void buttonOK() {
		GridPane grid = (GridPane) ((BorderPane) Main.EraseEdgeStage.getScene().getRoot()).getCenter();
		int u = -1, v = -1;
		try {
			u = Integer.valueOf(((TextField) grid.getChildren().get(2)).getCharacters().toString());
			v = Integer.valueOf(((TextField) grid.getChildren().get(3)).getCharacters().toString());
			if (u < 0 || u >= Main.getCanvas().vertexNumber) 
				main.showError("Invalid start vertex !\nVertex must be an integer between 0 and " + (Main.getCanvas().vertexNumber - 1)); 
			else 
			if (v < 0 || v >= Main.getCanvas().vertexNumber) 
				main.showError("Invalid end vertex !\nVertex must be an integer between 0 and " + (Main.getCanvas().vertexNumber - 1)); 
			else
			if (u == v) 
				main.showError("Start vertex and End vertex must be different."); 
			else
			if (!Main.getCanvas().edges.contains(new Edge(u, v, 0))) 
				main.showError("There is no edge between " + u + " and " + v); 
			else {
				Optional<ButtonType> result = main.showConfirmation("The edge between " + u + " and " + v + " will be erased.\nAre you sure?");
				if (result.get() == ButtonType.OK) {
					Main.getCanvas().eraseEdge(u, v, 0);
					Main.EraseEdgeStage.close();
				}
			}
		}
		catch (Exception e) {
			if (u < 0 || u >= Main.getCanvas().vertexNumber) 
				main.showError("Invalid start vertex !\nVertex must be an integer between 0 and " + (Main.getCanvas().vertexNumber - 1)); 
			else
			if (v < 0 || v >= Main.getCanvas().vertexNumber) 
				main.showError("Invalid end vertex !\nVertex must be an integer between 0 and " + (Main.getCanvas().vertexNumber - 1)); 
			else
			if (u == v) 
				main.showError("Start vertex and End vertex must be different."); 
			else
			main.showError("Erase edge error.");
		}
	}
	
	@FXML
	public void buttonCancel() {
		Main.EraseEdgeStage.close();
	}
	
	@FXML
	public void keyPressed(KeyEvent e) {
		if (e.getCode() == KeyCode.ENTER) {
			buttonOK();
		}
	}
}
