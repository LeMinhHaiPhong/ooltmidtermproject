package application;

import java.io.IOException;
import java.util.Optional;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.ButtonType;

public class MainController {
	
	Main main = new Main();
	
	@FXML
	public void exitApp() {
		Platform.exit();
		System.exit(0);
	}
	
	@FXML
	public void ButtonBFS() throws IOException, CloneNotSupportedException {
		if (Main.getCanvas().vertexNumber == 0) {
			main.showError("The graph is empty. Can't run BFS algorithm !");
		}
		else
		main.showBfsView();
	}
	
	@FXML
	public void ButtonDFS() throws IOException {
		if (Main.getCanvas().vertexNumber == 0) {
			main.showError("The graph is empty. Can't run DFS algorithm !");
		}
		else
		main.showDfsView();
	}
	
	@FXML
	public void ButtonDijkstra() throws IOException {
		if (Main.getCanvas().vertexNumber == 0) {
			main.showError("The graph is empty. Can't run Dijkstra algorithm !");
		}
		else
		main.showDijkstraView();
	}
	
	@FXML
	public void clearGraph() {
		Optional<ButtonType> result = main.showConfirmation("Are you sure you want to clear the graph ? All vertices and edges will be lost.");
		if (result.get() == ButtonType.OK) {
			Main.getCanvas().clearGraph();
		}
	}
	
	@FXML
	public void showAddEdgeView() throws IOException {
		if (Main.getCanvas().vertexNumber < 2) main.showError("There are less than 2 vertices in the graph."); else
		main.showAddEdgeView();
	}
	
	@FXML
	public void showEraseEdgeView() throws IOException {
		if (Main.getCanvas().edges.isEmpty()) main.showError("There are no edge in the graph."); else
		main.showEraseEdgeView();
	}
	
	@FXML
    public void showGuideView() throws IOException {
		main.showGuideView();
    }

    @FXML
    public void showAboutView() throws IOException {
    	main.showAboutView();
    }
	
}
