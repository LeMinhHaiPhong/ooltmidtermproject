package application.algorithm;

import application.Main;
import javafx.scene.text.Text;

public class MyNode extends Text implements Comparable<MyNode> {

	private int node = 0;
	private int cost = 0;
	
	public MyNode() {
		super();
	}
	
	public MyNode(int node, int cost) {
		this();
		this.setNode(node);
		this.setCost(cost);
	}
	
	public int getNode() {
		return node;
	}

	public void setNode(int node) {
		this.node = node;
		this.setX(Main.getCanvas().vertices[node].getx());
		this.setY(Main.getCanvas().vertices[node].gety() + 20);
	}

	public int getCost() {
		return cost;
	}

	public void setCost(int cost) {
		this.cost = cost;
		this.setText(Integer.toString(cost));
	}

	@Override
	public int compareTo(MyNode o) {
		if (this.cost != o.cost) return (this.cost < o.cost) ? -1 : 1;
		if (this.node != o.node) return (this.node < o.node) ? -1 : 1;
		return 0;
	}
	
	@Override
	public boolean equals(Object o) {
		return this.node == ((MyNode) o).getNode() && this.cost == ((MyNode) o).getCost();
	}
	
}
