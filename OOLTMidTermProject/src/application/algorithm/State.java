package application.algorithm;

import java.util.PriorityQueue;

public class State {
	public int x;
	public int y;
	public int dist[];
	public int parent[];
	public PriorityQueue<MyNode> heap = new PriorityQueue<MyNode>();
	
};
