package application.algorithm;

import java.util.Arrays;

import application.Main;
import application.graph.Edge;
import application.graph.Vertex;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Duration;

public abstract class Algorithm {
	
	protected static BorderPane _root;
	protected static BorderPane _right;
	protected static AnchorPane _Canvas;
	protected static HBox _buttonBox;
	protected static VBox _pseudocode;
	protected static VBox _watchArea;
	protected static VBox _solution;
	protected static Button _ButtonPlayOrPause;
	
	protected static int nVertices;
	protected static Vertex vertices[];
	protected static int start;
	protected static boolean weighted;
	protected static int connect[][];
	protected static int dist[];
	protected static int parent[];
	protected static State steps[];
	protected static int maxSteps;
	protected static int currentStep;
	protected static Timeline timeline;
	
	public Algorithm() {

	}
	
	public Algorithm(Stage stage, boolean weighted) {
		super();
		_root = (BorderPane) stage.getScene().getRoot();
		_right = (BorderPane) _root.getRight();
		_buttonBox = (HBox) _right.getTop();
		_pseudocode = (VBox) _right.getCenter();
		_watchArea = (VBox) _root.getBottom();
		_solution = (VBox) _root.getLeft();
		_ButtonPlayOrPause = (Button) _buttonBox.getChildren().get(2);
		
		Algorithm.weighted = weighted;
		getMainCanvas(weighted);
		start = Integer.valueOf(Main.startVertex.getText());
	}
	
	public void playOrPause() {
		if (timeline.getStatus() != Animation.Status.RUNNING) {
			_ButtonPlayOrPause.setText("Pause");
			if (currentStep >= maxSteps - 1) {
				currentStep = -1;
				runStep(-1);
			}
			timeline.play();
		}
		else {
			_ButtonPlayOrPause.setText("Play");
			timeline.stop();
		}
	}
	
	public void back() {
		if (currentStep < 0) return;
		currentStep--;
		runStep(currentStep);
		if (timeline.getStatus() == Animation.Status.RUNNING) {
			timeline.stop();
			timeline.play();
		}
	}
	
	public void backToFirst() {
		currentStep = -1;
		runStep(currentStep);
		if (timeline.getStatus() == Animation.Status.RUNNING) {
			timeline.stop();
			timeline.play();
		}
	}
	
	public void next() {
		if (currentStep >= maxSteps - 1) return;
		currentStep++;
		runStep(currentStep);
		if (timeline.getStatus() == Animation.Status.RUNNING) {
			timeline.stop();
			timeline.play();
		}
	}
	
	public void nextToLast() {
		currentStep = maxSteps - 1;
		runStep(currentStep);
	}
	
	public boolean showSolution(MouseEvent e) {
		if (timeline.getStatus() == Animation.Status.RUNNING) playOrPause();
		for(int i = 0; i < nVertices; i++) {
			double dist = (e.getX() - vertices[i].getx()) * (e.getX() - vertices[i].getx()) + (e.getY() - vertices[i].gety()) * (e.getY() - vertices[i].gety());
    		if (dist < 100) {
    			displaySolution(i);
    			return true;
    		}
		}
		return false;
	}
	
	public void displaySolution(int i) {
		ResetColorGraph();
		_solution.getChildren().clear();
		_solution.getChildren().add(new Text("Find Solution:"));
		_solution.getChildren().add(new Text("Source: " + start));
		_solution.getChildren().add(new Text("Target: " + i));
		IntegerProperty sum = new SimpleIntegerProperty();
		StringProperty _path = new SimpleStringProperty();
		sum.set(0);
		if (backtrack(start, i, currentStep, sum, _path)) {
			_solution.getChildren().add(new Text("Total distance: " + sum.get()));
			TextArea _area = new TextArea("Path: " + _path.get());
			_area.setEditable(false);
			_area.setWrapText(true);
			_solution.getChildren().add(_area);
		}
		else {
			TextArea _area = new TextArea("The target has not been visited or is disconnected from the source.	");
			_area.setEditable(false);
			_area.setWrapText(true);
			_solution.getChildren().add(_area);
		}
	}
	
	public boolean backtrack(int start, int finish, int step, IntegerProperty sum, StringProperty _path) {
		boolean ok = false;
		_path.set(String.valueOf(finish));
		if (finish == start && step > -1) {
			changeColorVertex(finish, Color.RED); 
			ok = true;
		} else changeColorVertex(finish, Color.BLACK);
		
		while (step >= 0 && finish >= 0 && finish != start) {
			ok = true;
			int p = steps[step].parent[finish];
			if (p < 0) return false;
			sum.set(sum.get() + connect[p][finish]);
			_path.set(p + " -> " + _path.get());
			if (p == start) {
				changeColorVertex(p, Color.RED);
				break;
			}
			else {
				changeColorVertex(p, Color.YELLOW);
			}
			finish = p;
		}
		return ok;
	}
	
	public void demonstrate() {
		currentStep = -1;
		runStep(-1);
		timeline = new Timeline(
		    new KeyFrame(
		        Duration.seconds(1),
		        event -> {
		        	runStep(++currentStep);
		        	if (currentStep >= maxSteps - 1) {
		    			Button _ButtonPlayOrPause = (Button) _buttonBox.getChildren().get(2);
		        		_ButtonPlayOrPause.setText("Play");
	        			timeline.stop();
		        	}
		        }
		    )
		);
		timeline.setCycleCount(Animation.INDEFINITE);
	}
	
	public void runStep(int step) {
		if (step >= maxSteps) return;
		ResetColorGraph();
		if (step < 0) return;
		Label _label = (Label) ((HBox) _root.getTop()).getChildren().get(1);
		_label.setText("" + (step + 1));
		runTasks(step);
		
		if (step == maxSteps - 1) {
			_ButtonPlayOrPause.setText("Play");
			timeline.stop();
		}
	}
	
	public abstract void runTasks(int step);
	
	public void getMainCanvas(boolean weighted) {
		nVertices = Main.getCanvas().getVertexNumber();
		vertices = new Vertex[nVertices];
		_Canvas = (AnchorPane) _root.getCenter();
		for (int i = 0; i < nVertices; i++) {
			vertices[i] = Main.getCanvas().getVertex(i);
			_Canvas.getChildren().add(vertices[i]);
		}
		
		connect = new int[nVertices][nVertices];
		for (int[] _connect : connect) Arrays.fill(_connect, -1);
		for (Edge e : Main.getCanvas().edges) {
			Edge _e = e.clone();
			_Canvas.getChildren().add(0, _e);
			if (!weighted) _e.getChildren().remove(_e.weightText);
			connect[e.getU()][e.getV()] = (weighted) ? _e.getWeight() : 1;
			connect[e.getV()][e.getU()] = (weighted) ? _e.getWeight() : 1;
		}
	}
	
	protected void ResetColorGraph() {
		for (int i = 0; i < nVertices; i++) {
			changeColorVertex(i, Color.WHITE);
		}
	}
	
	protected void changeColorVertex(int x, Color color) {
		((Circle) vertices[x].getChildren().get(0)).setFill(color);
	}
	
	protected abstract void changeColorGraph(int step);
	
	protected void ResetColorPseudocode() {
		for (int i = 0; i < _pseudocode.getChildren().size(); i++) {
			changeColorPseudocode(i, "white");
		}
	}
	
	protected void changeColorPseudocode(int x, String color) {
		_pseudocode.getChildren().get(x).setStyle("-fx-background-color:" + color);
	}
	
	protected abstract void changeColorPseudocode(int step);
	
//	protected void updateWatchArea(TextArea _area, String name, int... a) {
//		_area.setText(fromArray(name, a));
//	}
//	
//	protected String fromArray(String arrayName, int... a) {
//		String res = "";
//		System.out.println("ok");
//		for (int i = 0; i < a.length; i++) {
//			if (i > 0) res += ", ";
//			res += String.valueOf(a[i]);
//		}
//		return arrayName + " = { " + res + " }";
//	}
	
}
