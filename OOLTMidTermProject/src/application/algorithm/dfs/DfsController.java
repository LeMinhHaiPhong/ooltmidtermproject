package application.algorithm.dfs;

import java.util.Arrays;

import application.algorithm.Algorithm;
import application.algorithm.State;
import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class DfsController extends Algorithm {

	
	public DfsController() {
		super();
	}
	
	public DfsController(Stage stage) {
		super(stage, false);
		// TODO Auto-generated constructor stub
	}
	
	@FXML
	public void playOrPause() {
		super.playOrPause();
	}
	
	@FXML
	public void back() {
		super.back();
	}
	
	@FXML
	public void backToFirst() {
		super.backToFirst();
	}
	
	@FXML
	public void next() {
		super.next();
	}
	
	@FXML
	public void nextToLast() {
		super.nextToLast();
	}
	
	@FXML
	public boolean showSolution(MouseEvent e) {
		return super.showSolution(e);
	}
	
	public void runTasks(int step) {
		changeColorGraph(step);
		changeTextAreaU(step);
		changeTextAreaV(step);
		changeTextAreaPreOrdering(step);
		changeTextAreaPostOrdering(step);
		changeTextAreaDepth(step);
	}
	
	protected void changeColorGraph(int step) {
		
		for (int i = 0; i <= step; i++) {
			int u = steps[i].x;
			if (steps[i].y == 0) {
				changeColorVertex(u, Color.INDIANRED);
			}
			else {
				changeColorVertex(u, Color.YELLOW);
			}
		}
	}
	
	protected void changeColorPseudocode(int step) {
		ResetColorPseudocode();
		if (step > -1) {
			if (step == 0) {
				changeColorPseudocode(1, "IndianRed");
				changeColorPseudocode(2, "IndianRed");
				changeColorPseudocode(3, "IndianRed");
			}
			else
				if (steps[step].y == 0) {
					changeColorPseudocode(8, "IndianRed");
					changeColorPseudocode(9, "IndianRed");
					changeColorPseudocode(10, "IndianRed");
				}
				else {
					changeColorPseudocode(11, "Yellow");
				}
		}
	}
	
	private void changeTextAreaU(int step) {
		TextArea _text = (TextArea) _watchArea.getChildren().get(0);
		int u = -1;
		if (step > 0) {
			if (steps[step].y == 0) u = parent[steps[step].x]; else u = steps[step].x;
		}
		_text.setText("u = " + u);
	}
	
	private void changeTextAreaV(int step) {
		TextArea _text = (TextArea) _watchArea.getChildren().get(1);
		int v = -1;
		while (step > 0 && steps[step].y == 1) step--;
		if (step > 0) v = steps[step].x;
		_text.setText("v = " + v);
	}
	
	private void changeTextAreaPreOrdering(int step) {
		TextArea _text = (TextArea) _watchArea.getChildren().get(2);
		String s = "";
		if (step > -1) {
			for (int i = 0; i <= step; i++)
				if (steps[i].y == 0) {
					if (!s.isEmpty()) s += " , ";
					s += steps[i].x;
				}
		}
		_text.setText("PreOrdering = " + "{ " + s + " }");
	}
	
	private void changeTextAreaPostOrdering(int step) {
		TextArea _text = (TextArea) _watchArea.getChildren().get(3);
		String s = "";
		if (step > -1) {
			for (int i = 0; i <= step; i++)
				if (steps[i].y == 1) {
					if (!s.isEmpty()) s += " , ";
					s += steps[i].x;
				}
		}
		_text.setText("PostOrdering = " + "{ " + s + " }");
	}
	
	private void changeTextAreaDepth(int step) {
		TextArea _text = (TextArea) _watchArea.getChildren().get(4);
		String s = "";
		if (step > -1) {
			for (int i = 0; i <= step; i++)
				if (steps[i].y == 0) {
					if (!s.isEmpty()) s += " , ";
					s += "([" + steps[i].x + "] = " + dist[steps[i].x] + ")";
				}
		}
		_text.setText("Depth = " + "{ " + s + " }");
	}
	
	public void runDFS() {
		dist = new int[nVertices];
		Arrays.fill(dist, -1);
		parent = new int[nVertices];
		Arrays.fill(parent, -1);
		steps = new State[nVertices * 2];
		dist[start] = 0;
		parent[start] = -2;
		maxSteps = 0;
		DFS(start);
		demonstrate();
	}
	
	
	private void DFS(int u) {
		updateState(u, 0);
		for (int v = 0; v < nVertices; v++) {
			if (connect[u][v] >= 0 && dist[v] == -1) {
				dist[v] = dist[u] + 1;
				parent[v] = u;
				DFS(v);
			}
		}
		updateState(u, 1);
	}
	
	public void updateState(int x, int y) {
		steps[maxSteps] = new State();
		steps[maxSteps].x = x;
		steps[maxSteps].y = y;
		steps[maxSteps].dist = dist.clone();
		steps[maxSteps].parent = parent.clone();
		maxSteps++;
	}
	
}
