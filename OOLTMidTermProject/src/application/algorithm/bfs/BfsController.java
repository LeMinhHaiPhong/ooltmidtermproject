package application.algorithm.bfs;

import java.util.Arrays;

import application.algorithm.Algorithm;
import application.algorithm.State;
import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class BfsController extends Algorithm {

	private static int queue[];
	private static int queueBot;
	private static int queueTop;
	private static boolean queued[];
	
	public BfsController() {
		super();
	}
	
	public BfsController(Stage stage) {
		super(stage, false);
	}
	
	@FXML
	public void playOrPause() {
		super.playOrPause();
	}
	
	@FXML
	public void back() {
		super.back();
	}
	
	@FXML
	public void backToFirst() {
		super.backToFirst();
	}
	
	@FXML
	public void next() {
		super.next();
	}
	
	@FXML
	public void nextToLast() {
		super.nextToLast();
	}
	
	@FXML
	public boolean showSolution(MouseEvent e) {
		return super.showSolution(e);
	}
	
	public void runTasks(int step) {
		changeColorGraph(step);	
		changeTextFieldU(step);
		changeTextFieldV(step);
		changeColorPseudocode(step);
		changeQueue(step);
		changeOutOfQueue(step);
		changeVisited(step);
		changeDistance(step);
	}
	
	protected void changeColorGraph(int step) {
		for (int i = 0; i <= step; i++) {
			int u = steps[i].x;
			if (steps[i].y == 0) {
				changeColorVertex(u, Color.INDIANRED);
			}
			else {
				changeColorVertex(u, Color.YELLOW);
			}
		}
	}
	
	protected void changeColorPseudocode(int step) {
		ResetColorPseudocode();
		if (step == 0) {
			changeColorPseudocode(2, "IndianRed");
			changeColorPseudocode(3, "IndianRed");
			changeColorPseudocode(4, "IndianRed");
		} 
		else
		if (step > -1) {
			if (steps[step].y == 1) {
				changeColorPseudocode(6, "Yellow");
			}
			else {
				changeColorPseudocode(9, "IndianRed");
				changeColorPseudocode(10, "IndianRed");
				changeColorPseudocode(11, "IndianRed");
			}
		}	
	}
	
	private void changeTextFieldU(int step) {
		while (step > 0 && steps[step].y != 1) step--;
		int u = -1;
		if (step > 0) u = steps[step].x;
		TextArea _text = (TextArea) _watchArea.getChildren().get(0);
		_text.setText("u = " + u);
	}
	
	private void changeTextFieldV(int step) {
		while (step > 0 && steps[step].y != 0) step--;
		int v = -1;
		if (step > 0) v = steps[step].x;
		TextArea _text = (TextArea) _watchArea.getChildren().get(1);
		_text.setText("v = " + v);
	}
	
	private void changeQueue(int step) {
		queueBot = 0; 
		queueTop = 0;
		for (int i = 0; i <= step; i++) {
			if (steps[i].y == 0) queue[queueTop++] = steps[i].x; else queueBot++;
		}
		
		String s = "";
		for (int i = queueBot; i < queueTop; i++) {
			if (i > queueBot) s += ", ";
			s = s + queue[i];
		}
		
		TextArea _text = (TextArea) _watchArea.getChildren().get(2);
		_text.setText("Queue = {" + s + " }");
	}
	
	private void changeOutOfQueue(int step) {
		queueBot = 0; 
		queueTop = 0;
		for (int i = 0; i <= step; i++) {
			if (steps[i].y == 0) queue[queueTop++] = steps[i].x; else queueBot++;
		}
		
		String s = "";
		for (int i = 0; i < queueBot; i++) {
			if (!s.isEmpty()) s += ", ";
			s = s + queue[i];
		}
		
		TextArea _text = (TextArea) _watchArea.getChildren().get(3);
		_text.setText("Out of Queue = { " + s + " }");
	}
	
	private void changeVisited(int step) {
		String s = "";
		for (int i = 0; i <= step; i++)
			if (steps[i].y == 0) {
				if (!s.isEmpty()) s += ", ";
				s = s + steps[i].x;
			}
		
		TextArea _text = (TextArea) _watchArea.getChildren().get(4);
		_text.setText("Visited = { " + s + " }");
	}
	
	private void changeDistance(int step) {
		String s = "";
		if (step > -1) {
			for (int i = 0; i <= step; i++)
				if (steps[i].y == 0) {
					if (!s.isEmpty()) s += " , ";
					s += "([" + steps[i].x + "] = " + dist[steps[i].x] + ")";
				}
		}
		TextArea _text = (TextArea) _watchArea.getChildren().get(5);
		_text.setText("Distance from source = { " + s + " }");
	}
	
	public void runBFS() {
		steps = new State[nVertices * 2];
		queue = new int[nVertices + 1];
		queued = new boolean[nVertices];
		dist = new int[nVertices];
		parent = new int[nVertices];
		Arrays.fill(queued, false);
		Arrays.fill(dist, Integer.MAX_VALUE);
		Arrays.fill(parent, -1);
		maxSteps = 0;
		queueBot = 0;
		queueTop = 0;
		BFS(start);
		demonstrate();
	}
	
	public void BFS(int start) {
		dist[start] = 0;
		parent[start] = -2;
		enQueue(start);
		while (queueBot < queueTop) {
			int u = getBotQueue();
			deQueue();
			for (int v = 0; v < nVertices; v++)
			if (connect[u][v] >= 0 && !queued[v]) {
				dist[v] = dist[u] + 1;
				parent[v] = u;
				enQueue(v);
			}
		}
	}
	
	private void enQueue(int u) {
		queued[u] = true;
		queue[queueTop++] = u;
		updateState(u, 0);
	}
	
	private void deQueue() {
		updateState(queue[queueBot], 1);
		queueBot++;
	}
	
	private int getBotQueue() {
		return queue[queueBot];
	}
	
	public void updateState(int x, int y) {
		steps[maxSteps] = new State();
		steps[maxSteps].x = x;
		steps[maxSteps].y = y;
		steps[maxSteps].dist = dist.clone();
		steps[maxSteps].parent = parent.clone();
		maxSteps++;
	}
	
}
