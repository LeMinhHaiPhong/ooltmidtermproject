package application.algorithm.dijkstra;

import java.util.Arrays;
import java.util.Iterator;
import java.util.PriorityQueue;

import application.algorithm.Algorithm;
import application.algorithm.MyNode;
import application.algorithm.State;
import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class DijkstraController extends Algorithm {

	private static PriorityQueue<MyNode> heap;
	
	public DijkstraController() {
		super();
	}
	
	public DijkstraController(Stage stage) {
		super(stage, true);
		// TODO Auto-generated constructor stub
	}
	
	@FXML
	public void playOrPause() {
		super.playOrPause();
	}
	
	@FXML
	public void back() {
		super.back();
	}
	
	@FXML
	public void backToFirst() {
		super.backToFirst();
	}
	
	@FXML
	public void next() {
		super.next();
	}
	
	@FXML
	public void nextToLast() {
		super.nextToLast();
	}
	
	@FXML
	public boolean showSolution(MouseEvent e) {
		return super.showSolution(e);
	}
	
	public void runTasks(int step) {
		changeColorGraph(step);
		changeTextFieldU(step);
		changeTextFieldV(step);
		changeColorPseudocode(step);
		changeHeap(step);
		changeOutOfHeap(step);
		changeVisited(step);
		changeDistance(step);
	}
	
	protected void changeColorGraph(int step) {
		ResetColorGraph();
		for (int i = 0; i <= step; i++) {
			int u = steps[i].x;
			if (steps[i].y == 0) {
				changeColorVertex(u, Color.INDIANRED);
			}
			else
			if (steps[i].y == 1) {
				changeColorVertex(u, Color.YELLOW);
			}
			else {
				changeColorVertex(u, Color.RED);
			}
		}
	}
	
	protected void changeColorPseudocode(int step) {
		ResetColorPseudocode();
		if (step == 0) {
			changeColorPseudocode(2, "IndianRed");
			changeColorPseudocode(3, "IndianRed");
			changeColorPseudocode(4, "IndianRed");
		} 
		else
		if (step > -1) {
			if (steps[step].y == 1) {
				changeColorPseudocode(6, "Yellow");
			}
			else
			if (steps[step].y == 0) {
				changeColorPseudocode(9, "IndianRed");
				changeColorPseudocode(10, "IndianRed");
				changeColorPseudocode(11, "IndianRed");
			}
			else {
				changeColorPseudocode(13, "Red");
				changeColorPseudocode(14, "Red");
			}
		}	
	}
	
	private void changeTextFieldU(int step) {
		while (step > 0 && steps[step].y != 1) step--;
		int u = -1;
		if (step > 0) u = steps[step].x;
		TextArea _text = (TextArea) _watchArea.getChildren().get(0);
		_text.setText("u = " + u);
	}
	
	private void changeTextFieldV(int step) {
		while (step > 0 && steps[step].y % 2 != 0) step--;
		int v = -1;
		if (step > 0) v = steps[step].x;
		TextArea _text = (TextArea) _watchArea.getChildren().get(1);
		_text.setText("v = " + v);
	}
	
	private void changeHeap(int step) {
		String s = "";
		if (step >= 0) {
			Iterator<MyNode> i = steps[step].heap.iterator();
			while (i.hasNext()) {
				MyNode u = i.next();
				if (steps[step].dist[u.getNode()] == u.getCost()) {
					if (!s.isEmpty()) s += ", ";
					s += "(" + u.getNode() + ", " + u.getCost() + ")";
				}
			}
		}
		TextArea _text = (TextArea) _watchArea.getChildren().get(2);
		_text.setText("Heap = { " + s + " }");
	}
	
	private void changeOutOfHeap(int step) {
		String s = "";
		if (step >  -1) {
			for (int i = 0; i <= step; i++)
				if (steps[i].y == 1)  {
					if (!s.isEmpty()) s += ", ";
					s = s + steps[i].x;
				}
		}
		TextArea _text = (TextArea) _watchArea.getChildren().get(3);
		_text.setText("Out of Heap = { " + s + " }");
	}
	
	private void changeVisited(int step) {
		String s = "";
		if (step > -1) {
			for (int i = 0; i <= step; i++)
				if (steps[i].y == 0) {
					if (!s.isEmpty()) s += ", ";
					s = s + steps[i].x;
				}
		}
		TextArea _text = (TextArea) _watchArea.getChildren().get(4);
		_text.setText("Visited = { " + s + " }");
	}
	
	private void changeDistance(int step) {
		String s = "";
		if (step > -1) {
			for (int i = 0; i <= step; i++)
				if (steps[i].y == 0) {
					if (!s.isEmpty()) s += " , ";
					s += "([" + steps[i].x + "] = " + dist[steps[i].x] + ")";
				}
		}
		TextArea _text = (TextArea) _watchArea.getChildren().get(5);
		_text.setText("Distance from source = { " + s + " }");
	}
	
	public void runDijkstra() {
		steps = new State[nVertices * nVertices + nVertices];
		maxSteps = 0;
		heap = new PriorityQueue<MyNode>();
		dist = new int[nVertices];
		parent = new int[nVertices];
		Arrays.fill(dist, Integer.MAX_VALUE);
		Arrays.fill(parent, -1);
		Dijkstra(start);
		demonstrate();
	}
	
	private void Dijkstra(int start) {
		dist[start] = 0;
		parent[start] = -2;
		heap.add(new MyNode(start, 0));
		updateState(start, 0);
		while (!heap.isEmpty()) {
			MyNode u = heap.poll();
			if (u.getCost() != dist[u.getNode()]) continue;
			updateState(u.getNode(), 1);
			for (int v = 0; v < nVertices; v++) {
				if (connect[u.getNode()][v] > 0) {
					if (dist[v] == Integer.MAX_VALUE) {
						dist[v] = u.getCost() + connect[u.getNode()][v];
						parent[v] = u.getNode();
						heap.add(new MyNode(v, dist[v]));
						updateState(v, 0);
					}
					else
					if (dist[v] > u.getCost() + connect[u.getNode()][v]) {
						heap.remove(new MyNode(v, dist[v]));
						dist[v] = u.getCost() + connect[u.getNode()][v];
						parent[v] = u.getNode();
						heap.add(new MyNode(v, dist[v]));
						updateState(v, 2);
					}
				}
			}
		}
	}
	
	public void updateState(int x, int y) {
		steps[maxSteps] = new State();
		steps[maxSteps].x = x;
		steps[maxSteps].y = y;
		steps[maxSteps].dist = dist.clone();
		steps[maxSteps].parent = parent.clone();
		steps[maxSteps].heap = new PriorityQueue<MyNode>();
		Iterator<MyNode> i = heap.iterator();
		while (i.hasNext()) {
			MyNode u = i.next();
			steps[maxSteps].heap.add(u);
		}
		maxSteps++;
	}

}
